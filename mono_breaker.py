import sys
import string
import random
import argparse
import math

letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def swap(s, i, j):
    lst = list(s)
    lst[i], lst[j] = lst[j], lst[i]
    return ''.join(lst)

class get_corpus_grams:
    """
        file_name (type: string): the file containing n-grams and and their frequency seperated by a space
        ex)
            TION 52321
            NTHE 23122
            ...
    """
    def __init__(self, file_name):
        self.dic = {}
        self.total = 0

        with open(file_name,'r') as f:
            lines = f.readlines()

        for line in lines:
            l = line.split()
            gram = l[0]
            count = int(l[1])
            self.dic[gram] = count
            self.total += count

class get_text_grams:
    """
        text (type: string): the text where n-grams frequeny distribution will be extracted 
        n (type: int): n-gram size
    """
    def __init__(self, text, n):
        self.dic = {}
        self.total = 0

        for i in range(len(text)-n+1):
            gram = text[i:i+n]
            if gram in self.dic:
                self.dic[gram] += 1
            else:
                self.dic[gram] = 1

            self.total += 1

# Calculate the distance between two frequency distributions; one from English corpus, another from the text to be analyzed.
def cal_distance(corpus_dic, corpus_total, text_dic, text_total):
    distance = 0

    for gram, count in text_dic.items():
        if gram not in corpus_dic:
            corpus_prob = 0
        else:
            corpus_prob = corpus_dic[gram] / corpus_total
            
        text_prob = count / text_total

        if corpus_prob == 0:
            corpus_prob_log = math.log(sys.float_info.min)
        else:
            corpus_prob_log = math.log(corpus_prob)

        if text_prob == 0:
            text_prob_log = math.log(sys.float_info.min)
        else:
            text_prob_log = math.log(text_prob)

        distance += abs(corpus_prob_log - text_prob_log)
        # a different way to calculate distance:
        #distance += abs(corpus_prob_log)

    return distance

def hill_climb(ciphertext, corpus_grams):
    """
        Rough description of how this function works: 

        1. Select an initial key randomly
        2. Calculate current distance to the frequency distribution of English corpus
        3. Calculate neighbors' (i.e., neighbor keys) distances
        4. Pick the best neighbor (or any better neighbor). If no better neighbor, stop and return the current key and distance.
        5. Make a move (i.e. Current <- the best (or better) neighbor), then GOTO Step 3.
    """
    # Select an initial key randomly
    current_key = ''.join(random.sample(letters,len(letters)))
 
    current_decrypted = ciphertext.translate(str.maketrans(letters,current_key))
    current_grams = get_text_grams(current_decrypted, n_size)

    current_distance = cal_distance(corpus_grams.dic, corpus_grams.total, current_grams.dic, current_grams.total)

    if verbose:
        print ("Initial Key: ", current_key)
        print ("Initial Distance: ", current_distance)
 
    numOfMove = 0

    """
    # Move to the best neighbor
    local_optima_distance = current_distance
    local_optima_key = current_key
    while True:
        for i in range(25):
            for j in range(i+1,26):
                neighbor_key = swap(current_key,i,j)
                neighbor_decrpted = ciphertext.translate(str.maketrans(letters,neighbor_key))
                neighbor_decrpted_grams = get_text_grams(neighbor_decrpted, n_size)
                neighbor_distance = cal_distance(corpus_grams.dic, corpus_grams.total, neighbor_decrpted_grams.dic, neighbor_decrpted_grams.total)

                if neighbor_distance < local_optima_distance:
                    local_optima_distance = neighbor_distance
                    local_optima_key = neighbor_key

        if local_optima_distance == current_distance:  # No neighbor closer than current
            if verbose:
                print ("# of move: ", numOfMove)
                print ("Local Optima: ", local_optima_distance)
                print ("Deciphered (local optima): ", ciphertext.translate(str.maketrans(letters,local_optima_key)), "\n")

            return local_optima_key, local_optima_distance

        # make a move
        current_distance = local_optima_distance
        current_key = local_optima_key
        numOfMove += 1
    """
    # Move to a better neighbor first meet (less greedy)
    while True:
        for i in range(25):
            for j in range(i+1,26):
                # Get the distances from the neighbors of the current key
                neighbor_key = swap(current_key,i,j)
                neighbor_decrpted = ciphertext.translate(str.maketrans(letters,neighbor_key))
                neighbor_decrpted_grams = get_text_grams(neighbor_decrpted, n_size)
                neighbor_distance = cal_distance(corpus_grams.dic, corpus_grams.total, neighbor_decrpted_grams.dic, neighbor_decrpted_grams.total)

                if neighbor_distance < current_distance:
                    # Make a move
                    current_distance = neighbor_distance
                    current_key = neighbor_key
                    numOfMove += 1
                    break
            else:
                continue

            break
        else:
            # Found a local optima
            print ("* Local Optima: ", current_distance)
            if verbose:
                print ("* # of Move: ", numOfMove)
                print ("* Deciphered (local optima): ", ciphertext.translate(str.maketrans(letters,current_key)))
            return current_key, current_distance

def main():
    parser = argparse.ArgumentParser(description="Break Monoalphabetic Substitution Cipher")
    parser.add_argument('cipher_file', help='ciphertext file name')
    parser.add_argument("ngram_size", type=int, help='size of ngram')
    parser.add_argument("-r", "--restart", type=int, help="number of restart for hill climbing (default=20)", default=20)
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")

    args = parser.parse_args()

    global verbose
    verbose = args.verbose

    global n_size
    n_size = args.ngram_size

    if n_size == 1:
        corpus_file_name = "english_unigrams.txt"
    elif n_size == 2:
        corpus_file_name = "english_bigrams.txt"
    elif n_size == 3:
        corpus_file_name = "english_trigrams.txt"
    elif n_size == 4:
        corpus_file_name = "english_quadgrams.txt"
    elif n_size == 5:
        corpus_file_name = "english_quintgrams.txt"
    else:
        print ("Ooops! We don't have the letter frequency for {0}-gram".format(n_size))
        sys.exit()

    with open(args.cipher_file, 'r') as f:
        ciphertext = f.read()
    
    print ("Ciphertext: ", ciphertext, "\n")
    if verbose:
        cipher_grams = get_text_grams(ciphertext, n_size)
        #print (cipher_grams.dic)
        print ("Total # of {0}-grams in the ciphertext: {1}".format(n_size,cipher_grams.total))

    corpus_grams = get_corpus_grams(corpus_file_name)
    if verbose:
        print ("Total # of {0}-grams in the English Corpus: {1}".format(n_size, corpus_grams.total))

    local_optimas = []
    for i in range(args.restart):
        print ("\n[Start#{0}]".format(i))
        key, distance = hill_climb(ciphertext,corpus_grams)
        local_optimas.append((key,distance))

    global_optima_distance = sys.float_info.max
    for local_optima in local_optimas:
        if local_optima[1] < global_optima_distance:
            global_optima_distance = local_optima[1]
            global_optima_key = local_optima[0]

    deciphered = ciphertext.translate(str.maketrans(letters,global_optima_key))

    count = 0
    for local_optima in local_optimas:
        if local_optima[1] == global_optima_distance:
            count += 1
    print ("\n====== Result ======")
    print ("* (estimated) Global Optima: ", global_optima_distance)
    print ("* # of Start reached (estimated) Global Optima: ", count)
    print ("* Found Key: ", global_optima_key)
    print ("* Deciphered: ", deciphered)
    print ("\n")

if __name__ == '__main__':
    main()
