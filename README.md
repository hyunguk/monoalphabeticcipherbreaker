# Requirements
- Python3

# English Letter Frequencies Files
- english_unigrams.txt
- english_bigrams.txt
- english_trigrams.txt
- english_quadgrams.txt
- english_quintgrams.txt  
- Source: http://practicalcryptography.com/cryptanalysis/letter-frequencies-various-languages/english-letter-frequencies/

# Note:
In my experiments with the sample ciphertext, using trigrams or quadgrams has shown better performance.  
